module woff

go 1.16

require (
	github.com/go-chi/chi v1.5.4
	github.com/stripe/stripe-go/v72 v72.58.0
)
